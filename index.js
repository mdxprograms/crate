const express        = require('express');
const cors           = require('cors');
const app            = express();
const fs             = require('fs');
const path           = require('path');
const parser         = require('markdown-parse');
require('dotenv').config();
const {ENV, PORT}    = process.env;
const CONTENT_DIR    = `${__dirname}/content`;
const DATA_DIR       = `${__dirname}/data`;

app.use(cors());

if (ENV === 'production') {
  app.use(express.static('app/build'));
}

app.get('/', (req, res) => {
  let dir = 'public';
  if (ENV === 'production') {
    dir = 'build';
  }
  res.sendFile(`${__dirname}/app/${dir}/index.html`);
});

app.get('/api/content/:type', (req, res) => {
  let data = [];
  const contentFiles = fetchFiles(CONTENT_DIR);

  contentFiles.map((tree, i) => {
    const dir = Object.keys(tree)[0];
    const files = Object.keys(tree).map((key) => {
      return tree[key];
    })[0];

    if (fs.lstatSync(`${CONTENT_DIR}/${dir}`).isDirectory()
      && files.length) {

      files.map((f) => {
        const content = parseContent(dir, f);
        if (req.params.type && req.params.type === dir) {
          data.push(content);
        }
      });
    }
  });

  res.send(data);
});

const parseContent = (contentType, file) => {
  let output = {};
  const content = fs.readFileSync(`${__dirname}/content/${contentType}/${file}`, 'utf-8');

  parser(content, (err, result) => {
    output.body = result.body;
    for (let data in result.attributes) {
      output[data] = result.attributes[data];
    }
    output.url = output.permalink || output.title.replace(/ /g, '-').toLowerCase();
  });

  return output;
};

const fetchFiles = (dir, fileList = []) => {
  fs.readdirSync(dir).forEach(file => {
    const filePath = path.join(dir, file);

    fileList.push(
      fs.statSync(filePath).isDirectory()
        ? {[file]: fetchFiles(filePath)}
        : file
    );
  });

  return fileList;
};

app.listen(PORT, () => {
  console.log(`Crate listening on port ${PORT}`);
});
