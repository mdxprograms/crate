import React, {Component} from 'react';
import { Link } from 'react-router';
import { Card, CardTitle } from 'react-materialize';
import axios from 'axios';

class Posts extends Component {
  constructor() {
    super();
    this.state = {
      posts: []
    };
  }

  componentWillMount() {
    axios.get('/api/content/blog')
      .then((response) => {
        this.setState({posts: response.data});
      })
      .catch((err) => {
        console.error(err);
      })
  }

  render() {
    let posts = this.state.posts.map((p) => {
          return (
            <Card
              key={p.title}
              className="post small"
              header={<CardTitle image='assets/sample-1.jpg'>{p.title}</CardTitle>}
              actions={[<Link to={`/blog/${p.url}`}>Read More</Link>]}>
              <p>
                {p.body}
              </p>
            </Card>
          )
        });
    return (
      <div className="posts col s12 m6">
        {this.props.children}
        {posts}
      </div>
    );
  }
}

export default Posts;