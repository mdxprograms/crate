import React, {Component} from 'react';
import { Breadcrumb, MenuItem } from 'react-materialize';

class Breadcrumbs extends Component {
  render() {
    return (
      <Breadcrumb>
        <MenuItem>Home</MenuItem>
        <MenuItem>{location.pathname.replace('/', '')}</MenuItem>
      </Breadcrumb>
    );
  }
}

export default Breadcrumbs;