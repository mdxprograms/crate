import React, { Component } from 'react';
import { Link } from 'react-router';
import { Button, Icon, Dropdown, NavItem } from 'react-materialize';
import './styles.css';

class App extends Component {
  render() {
    return (
      <div className="app container">
        <Dropdown
          trigger={
            <Button>Dropdown<Icon right>arrow_drop_down</Icon></Button>
          }>
          <Link to={"/"}>Home</Link>
          <NavItem href='/blog'>Blog</NavItem>
        </Dropdown>
        <main className="content">
          {this.props.children}
        </main>
      </div>
    );
  }
}

export default App;
