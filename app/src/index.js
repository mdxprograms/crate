import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, browserHistory} from 'react-router';
import App from './components/App';
import Posts from './components/Posts';
import Post from './components/Post';
import './index.css';

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <Route path="/blog" component={Posts}>
        <Route path="/blog/:title" component={Post} />
      </Route>
    </Route>
  </Router>
), document.getElementById('root'));
